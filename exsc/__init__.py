# Thanks to uh_wot (https://gitlab.com/uh_wot) for helping me a lot with this bot.
# Basically this bot is a bad copy of his, but my version has lots of anime stuff, I also translated it to spanish.
from telegram.ext import Updater
import os

mode = "public" # set this to 'debug' to do polling (local hosting) instead of webhook

token = os.getenv("TOKEN")
port = os.getenv("PORT")
url = os.getenv("URL")

VERSION = "1.0"

updater = Updater(token)
dispatcher = updater.dispatcher
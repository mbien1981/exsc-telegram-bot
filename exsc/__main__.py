from . import token, url, port, updater, mode
from .modules import ALL_MODULES
import logging, importlib

logging.basicConfig(format='%(name)s - %(levelname)s - %(message)s', level=logging.INFO)

for module in ALL_MODULES:
	importlib.import_module("exsc.modules." + module) # note: you must replace 'exsc' in case you rename the folder.

# Setup web-hook
if mode != "debug":
	updater.start_webhook(listen="0.0.0.0", port=int(port), url_path=token)
	updater.bot.set_webhook(url + token)
else:
	updater.start_polling()
print("--","running", "--")
updater.idle()

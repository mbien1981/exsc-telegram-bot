# Honestly, I had no idea where to put this
slap_strings = {
	"templates": [
		"{user1} {hits} a {user2} con {item}.",
		"{user1} {hits} a {user2} en la cara con {item}.",
		"{user1} {throws} {item} hacia {user2}.",
		"{user1} coje {item} y lo {throws} hacia la cara de {user2}.",
		"{user1} lanza {item} a la direccion de {user2}.",
		"{user1} empieza a golpear {user2} con {item}.",
		"{user1} ata a {user2} en una silla y {throws} {item} hacia el.",
		"{user1} le da un pequeño empujón a {user2} para que aprenda a nadar en lava.",
		"{user1} usa SALPICADURA pero no pasa nada.",
		"{user1} golpea a {user2} con una espada de diamante."
	],
	"self": [
		"{user1} se golpea a si mismo en en la cara con {item}.",
		"{user1} golpea su cabeza contra el muro.",
		"{user1} se golpea a si mismo con {item} luego de ver la interfaz de Windows 8. Ha perdido la fe en la humanidad.",
	],
	"basic": [
		"{user1} se ha sofocado en un muro.",
		"{user1} fue aplastado por un piano."
	],
	"items": [
		"un bate de baseball",
		"un bate de cricket",
		"un palo de madera",
		"un clavo",
		"una pala",
		"un libro de física",
		"un tostador",
		"un retrato de Richard Stallman",
		"un televisor",
		"un camión de 5 toneladas",
		"un libro",
		"una laptop",
		"un televisor viejo",
		"un saco de rocas",
		"un pollo de goma",
		"un extintor",
		"un bollo de mierda",
		"un trozo de carne podrida",
		"un oso",
		"un montón de ladrillos",
		"un nokia 1100",
		"un bloque de tierra",
		"un dildo",
		"un caballo homosexual de las montañas"
	],
	"throw": [
		"arroja",
		"lanza",
		"tira"
	],
	"hit": [
		"golpea",
		"impacta",
		"abofetea"
	]
}

media = {
	"kiss_gifs": [
		"https://i.imgur.com/kzQPIwl.mp4",
		"https://i.imgur.com/8EMZcfP.mp4",
		"https://i.imgur.com/OE7lSSY.gif",
		"https://i.imgur.com/YkrEkbF.gif",
		"https://i.imgur.com/mNEHfJ0.gif",
		"https://i.imgur.com/0WWWvat.gif",
		"https://i.imgur.com/MzAjNdv.gif",
		"https://i.imgur.com/eKcWCgS.gif",
		"https://i.imgur.com/3aX4Qq2.gif",
		"https://i.imgur.com/uobBW9K.gif",
		"https://i.imgur.com/Esqtd1u.gif",
		"https://i.imgur.com/FozOXkB.gif",
		"https://i.imgur.com/7GhTplD.gif",
		"https://i.imgur.com/B6UKulT.gif",
		"https://i.imgur.com/Uow8no2.gif",
		"https://i.imgur.com/I159BUo.gif",
		"https://i.imgur.com/8YZFU1Z.gif",
		"https://i.imgur.com/uPtDEh6.gif",
		"https://i.imgur.com/pDScNqs.gif",
		"https://i.imgur.com/pXLBzEt.mp4",
		"https://i.imgur.com/1IuyOxK.gif",
		"https://i.imgur.com/nVM7Ll8.gif",
		"https://i.imgur.com/gLyYxIK.gif",
		"https://i.imgur.com/siVySzs.gif",
		"https://i.imgur.com/k2X9yOM.gif",
		"https://i.imgur.com/jlpmJSa.gif"
	],
	"hug_gifs": [
		"https://i.imgur.com/Ltmb8aa.gif",
		"https://i.imgur.com/rlOJqHL.mp4",
		"https://imgur.com/gallery/jWUi0Ph",
		"https://i.imgur.com/WaMqFur.mp4",
		"https://i.imgur.com/DhV27dy.gif",
		"https://i.imgur.com/gTbBI6U.gif",
		"https://i.imgur.com/mvDeiFR.gif",
		"https://i.imgur.com/oFPx1de.gif",
		"https://i.imgur.com/XgAgsMh.gif",
		"https://i.imgur.com/GeUDX8p.gif",
		"https://i.imgur.com/Bzp79lr.gif",
		"https://i.imgur.com/w5D0mNj.gif",
		"https://i.imgur.com/FmEj9v1.gif",
		"https://i.imgur.com/Sp9mc1q.gif",
		"https://i.imgur.com/RUWYjIq.gif",
		"https://i.imgur.com/jRZ8ii3.gif"
	],
	"pat_gifs": [
		"https://i.imgur.com/2048SO8.gif",
		"https://i.imgur.com/fhNU1gq.gif",
		"https://i.imgur.com/BRGrv3X.gif",
		"https://i.imgur.com/xER8JB1.gif",
		"https://i.imgur.com/PGyZGUt.gif",
		"https://i.imgur.com/LOTmJ5v.gif",
		"https://i.imgur.com/IivCZKf.gif",
		"https://i.imgur.com/qfLOaGM.gif",
		"https://i.imgur.com/PhILuNe.gif",
		"https://i.imgur.com/HMXCNrv.mp4",
		"https://i.imgur.com/WmQ3nSL.mp4",
		"https://i.imgur.com/hN3Xvwn.gif"
	],
	"shoot_gifs": [
		"https://i.imgur.com/XbNYaZ2.mp4"
	],
	"jposes": [
		"https://i.imgur.com/Gy2vxeH.mp4",
		"https://i.imgur.com/uPAVXYh.mp4",
		"https://i.imgur.com/kUby2cL.mp4",
		"https://i.imgur.com/3RZkRt8.mp4",
		"https://i.imgur.com/9s4zTxr.mp4",
		"https://i.imgur.com/hiePSu3.mp4",
		"https://i.imgur.com/kp5Bhi8.gif",
		"https://i.imgur.com/gFFqWsF.gif",
		"https://i.imgur.com/GmhnvgX.gif",
		"https://i.imgur.com/pM35WHo.mp4",
		"https://i.imgur.com/xsA5OEa.mp4",
		"https://i.imgur.com/A0D03EW.gif"
	]
}
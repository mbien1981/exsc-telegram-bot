import hashlib, math, os
import urllib.request as urllib
from io import BytesIO
from PIL import Image
from .. import dispatcher
from telegram import ParseMode, TelegramError, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import CommandHandler
from telegram.ext.dispatcher import run_async
from telegram.utils.helpers import escape_markdown
from typing import List

markdown = ParseMode.MARKDOWN

@run_async
def makepack_internal(msg, user, png_sticker, emoji, bot, packname):
	name = user.full_name[:50]
	try:
		success = bot.create_new_sticker_set(user.id, packname, "Stickers Favoritos | " + name, png_sticker=png_sticker, emojis=emoji)
	except TelegramError as e:
		if e.message == "Sticker set name is already occupied":
			bot.send_message(msg.chat.id, "Tu pack puede ser encontrado [aquí](t.me/addstickers/%s)" % packname, parse_mode=markdown)
		elif e.message == "Peer_id_invalid":
			return bot.send_message(msg.chat.id, "Primero escribeme al privado.", reply_markup=InlineKeyboardMarkup([[InlineKeyboardButton(text="Start", url=f"t.me/{bot.username}")]]))

	if success:
		bot.send_message(msg.chat.id, "Pack de stickers creado exitosamente, puedes encontrarlo [aquí](t.me/addstickers/%s)" % packname, parse_mode=markdown)
	else:
		bot.send_message(msg.chat.id, "No se ha podido crear el pack de stickers.")

@run_async
def Kang(bot, update, args: List[str]):
	msg, user, chat = update.effective_message, update.effective_user, update.effective_chat.id
	hash = hashlib.sha1(bytearray(user.id)).hexdigest()
	packname, kangsticker = f"a{hash[:20]}_kang_{bot.username}", "kangsticker.png"
	if msg.reply_to_message:
		if msg.reply_to_message.sticker:
			file_id = msg.reply_to_message.sticker.file_id
		elif msg.reply_to_message.photo:
			file_id = msg.reply_to_message.photo[-1].file_id
		elif msg.reply_to_message.document:
			file_id = msg.reply_to_message.document.file_id
		kang_file = bot.get_file(file_id)
		kang_file.download('kangsticker.png')
		if args:
			sticker_emoji = str(args[0])
		elif msg.reply_to_message.sticker and msg.reply_to_message.sticker.emoji:
			sticker_emoji = msg.reply_to_message.sticker.emoji
		else:
			sticker_emoji = "🖼"
		try:
			im, maxsize = Image.open(kangsticker), (512, 512)
			if (im.width and im.height) < 512:
				size1, size2 = im.width, im.height
				if im.width > im.height:
					scale, size1new, size2new = 512/size1, 512, size2 * scale
				else:
					scale, size1new, size2new = 512/size2, size1 * scale, 512
				size1new, size2new = math.floor(size1new), math.floor(size2new)
				sizenew = (size1new, size2new)
				im = im.resize(sizenew)
			else:
				im.thumbnail(maxsize)
			if not msg.reply_to_message.sticker:
				im.save(kangsticker, "PNG")
			bot.add_sticker_to_set(user_id=user.id, name=packname, png_sticker=open('kangsticker.png', 'rb'), emojis=sticker_emoji)
			bot.send_message(chat, "El sticker fue añadido exitosamente al [pack.](t.me/addstickers/{packname})\nEmoji: {sticker_emoji}", markdown)
		except OSError as e:
			return bot.send_message(chat, "Solo puedo robar imagenes...")
		except TelegramError as e:
			if e.message == "Stickerset_invalid":
				makepack_internal(msg, user, open('kangsticker.png', 'rb'), sticker_emoji, bot, packname)
			elif e.message == "Sticker_png_dimensions":
				im.save(kangsticker, "PNG")
				bot.add_sticker_to_set(user_id=user.id, name=packname, png_sticker=open('kangsticker.png', 'rb'), emojis=sticker_emoji)
				bot.send_message(chat, f"El sticker fue añadido exitosamente al [pack.](t.me/addstickers/{packname})\nEmoji: {sticker_emoji}", markdown)
			elif e.message == "¡Emoji inválido!":
				bot.send_message(chat, "Emoji(s) no válidos.")
			elif e.message == "Stickers_too_much":
				bot.send_message(chat, "Tu kang pack esta lleno.. ¡RATERO!")
	elif args:
		try:
			try:
				urlemoji = msg.text.split(" ")
				png_sticker, sticker_emoji = urlemoji[1], urlemoji[2]
			except IndexError:
				sticker_emoji = "🖼"
			urllib.urlretrieve(png_sticker, kangsticker)
			im, maxsize = Image.open(kangsticker), (512, 512)
			if (im.width and im.height) < 512:
				size1, size2 = im.width, im.height
				if im.width > im.height:
					scale, size1new, size2new = 512/size1, 512, size2 * scale
				else:
					scale, size1new, size2new = 512/size2, size1 * scale, 512
				size1new, size2new = math.floor(size1new), math.floor(size2new)
				sizenew = (size1new, size2new)
				im = im.resize(sizenew)
			else:
				im.thumbnail(maxsize)
			im.save(kangsticker, "PNG")
			msg.reply_photo(photo=open('kangsticker.png', 'rb'))
			bot.add_sticker_to_set(user_id=user.id, name=packname, png_sticker=open('kangsticker.png', 'rb'), emojis=sticker_emoji)
			bot.send_message(chat, f"El sticker ha sido añadido al [pack.](t.me/addstickers/{packname})\nEmoji: {sticker_emoji}", markdown)
		except OSError as e:
			return bot.send_message(chat, "Solo puedo robar imagenes...")
		except TelegramError as e:
			if e.message == "Stickerset_invalid":
				makepack_internal(msg, user, open('kangsticker.png', 'rb'), sticker_emoji, bot, packname)
			elif e.message == "Sticker_png_dimensions":
				bot.send_message(chat, "Could not resize image to the correct dimensions.")
			elif e.message == "Invalid sticker emojis":
				bot.send_message(chat, "¡Emoji Inválido!")
	else:
		bot.send_message(chat, f"¡Por favor responde a un sticker para poder añadirlo al pack!\npuedes encontrar tu pack [aquí](t.me/addstickers/{packname})", markdown)
	if os.path.isfile("kangsticker.png"):
		os.remove("kangsticker.png")

@run_async
def getsticker(bot, update):
	message, chat_id = update.effective_message, update.effective_chat.id
	if message.reply_to_message and message.reply_to_message.sticker:
		bot.get_file(message.reply_to_message.sticker.file_id).download('sticker.png')
		bot.send_document(chat_id, document=open('sticker.png', 'rb'))
		os.remove("sticker.png")
	else:
		pass

dispatcher.add_handler(CommandHandler("kang", Kang, pass_args=True))
dispatcher.add_handler(CommandHandler("getsticker", getsticker))

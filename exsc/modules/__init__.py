# This will automatically load all the files from the module.
from os.path import dirname, basename, isfile
import glob

mod_paths = glob.glob(dirname(__file__) + "/*.py")
ALL_MODULES = [basename(f)[:-3] for f in mod_paths if isfile(f) and f.endswith(".py") and not f.endswith('__init__.py')]

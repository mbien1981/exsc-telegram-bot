import asyncio, random, requests
from .. import dispatcher
from ..utils import *
from .data import media, slap_strings
from ..filters import _filters
from .database import database
from time import sleep as wait
from telegram import ParseMode
from telegram.ext import Filters, CommandHandler, run_async
from telegram.utils.helpers import escape_markdown
markdown = ParseMode.MARKDOWN

# Original code by Nick80835 (https://github.com/Nick80835/)
class danbooru():
	def __init__(self, loop, tag, rating):
		self.url = "http://danbooru.donmai.us/posts.json"
		self.params = {"limit": 1, "random": "true", "tags": f"Rating:{rating} {tag}"} # safe, questionable, explicit

	async def get_data(self):
		with requests.get(self.url, params=self.params) as response:
			if response.status_code == 200:
				response = response.json()
			else:
				raise Exception
			return response[0]

	async def find_image(self):
		data = await self.get_data()
		for url in ['file_url', 'large_file_url', 'source']:
			if url in data.keys():
				return data[url]

async def get_image(loop, tag, rating="questionable"):
	call = danbooru(loop, tag, rating)
	url = await call.find_image()
	return url

@run_async
def danbooru_request(bot, update):
	chat, message, loop = update.effective_chat, update.effective_message, asyncio.new_event_loop()
	if chat.type == "supergroup" and str(chat.id) in database['chats'] and not database['chats'][str(chat.id)]['settings']['allow_nsfw']:
		return bot.send_message(chat.id, "La opcion 'allow_nsfw' para este grupo no está activada!")

	if len((message.text).split()) > 1:
		if (message.text).split()[1] in _filters['blacklisted_tags']:
			tag = ""
		else:
			tag = (message.text).split()[1]
	else:
		tag = "" # random tag

	image_url = loop.run_until_complete(get_image(loop, tag))
	if tag == "gif":
		bot.send_video(chat.id, image_url, parse_mode=markdown)
	else:
		bot.send_photo(chat.id, image_url, f"[Original Image]({image_url})", parse_mode=markdown)
	loop.close()

@run_async
def roll_dice(bot, update):
	chat, message, user = update.effective_chat, update.effective_message, update.effective_user

	spt_txt, dados = message.text.split(), []
	dados.clear()

	if len(spt_txt) > 1:
		dados_totales = int(spt_txt[1])
	else:
		dados_totales = 1

	if len(spt_txt) > 2:
		caras = int(spt_txt[2])
	else:
		caras = 6

	if dados_totales >= 1 and dados_totales <= 400:
		for x in range(dados_totales):
			if caras >= 6 and caras <= 400:
				resultado = random.randint(1, caras)
			else:
				return bot.send_message(chat.id, "Cantidad invalida de caras.")
			dados.append(resultado)
	else:
		return bot.send_message(chat.id, "Cantidad invalida de dados.")

	if dados_totales > 1:
		roll_string = "Tirando {} dados de {} caras! 🎲".format(dados_totales, caras)
	else:
		roll_string = "Tirando un dado de {} caras! 🎲".format(caras)
	roll_message = bot.send_message(chat.id, roll_string, markdown)["message_id"]
	

	char_lst = ["[", "]"]
	total_text = str(dados)
	for i in char_lst:
		total_text = total_text.replace(i, "")
	total = sum(int(i) for i in dados)
	if dados_totales > 1:
		roll_total = "Los dados de {} fueron: {}! \nTotal: {}".format(user.full_name, total_text, total)
	else:
		roll_total  = "El dado de {} dió {}!".format(user.full_name, total_text)
	wait(3)
	bot.edit_message_text(roll_total, chat.id, roll_message)

@run_async
def random_choice(bot, update):
	chat, message = update.effective_chat, update.effective_message
	options = (message.text).split()
	options.remove('/random')
	bot.send_message(chat.id, random.choice(options))

class roulette():
	def __init__(self):
		self.chambers = 6
		self.selected_chamber = 1
		self.loaded_chamber = None

	def load(self):
		self.loaded_chamber = random.randint(1, self.chambers)
		self.selected_chamber = random.randint(1, self.chambers)
		return "{bot} toma el revolver, carga una bala en el tambor luego coloca lo sobre la mesa."

	def spin(self):
		if self.selected_chamber == self.chambers:
			self.selected_chamber = 0
		else:
			self.selected_chamber += 1

	def trigger(self):
		if self.loaded_chamber != None:
			if self.selected_chamber == self.loaded_chamber:
				self.loaded_chamber = None
				return "{user} toma el revolver y se lo coloca en la sien... ¡Bang!\n¡{user} ha muerto!\n{bot} coloca el revolver sobre la mesa."
			else:
				self.spin()
				return "{user} toma el revolver y se lo coloca en la sien... *click*\n{user} coloca el revolver sobre la mesa."
		else:
			return "¡El revolver no esta cargado!"

revolver = roulette()

@run_async
def _reload(bot, update):
	chat = update.effective_chat
	text = (revolver.load()).format(bot=mention(bot.get_me()))
	message = bot.send_message(chat.id, text, markdown)['message_id']
	wait(15)
	bot.delete_message(chat.id, message)

@run_async
def russian_roulette(bot, update):
	chat, user = update.effective_chat, update.effective_user
	text = (revolver.trigger()).format(user=mention(user), bot=mention(bot.get_me()))
	message = bot.send_message(chat.id, text, markdown)['message_id']
	wait(30)
	bot.delete_message(chat.id, message)

@run_async
def interact(bot, update):
	chat, message, user = update.effective_chat, update.effective_message, update.effective_user
	if (message.text).startswith("/kiss"):
		action = "ha besado a"
		gif = random.choice(media['kiss_gifs'])
	elif (message.text).startswith("/hug"):
		action = "ha abrazado a"
		gif = random.choice(media['hug_gifs'])
	elif (message.text).startswith('/shoot'):
		action = "toma el revolver y le dispara a"
		gif = random.choice(media['shoot_gifs'])

	if message.reply_to_message:
		rep = message.reply_to_message
		if rep.from_user.id == bot.id or rep.from_user.id == user.id:
			return
		text = f'{mention(user)} {action} {mention(rep.from_user)}.'
		bot.send_video(chat.id, gif, caption=text, parse_mode=markdown, reply_to_message_id=rep.message_id)#['message_id']
		#wait(15)
		# bot.delete_message(chat.id, msg)

@run_async
def jpose(bot, update):
	chat, message, user = update.effective_chat, update.effective_message, update.effective_user
	gif = random.choice(media['jposes'])
	bot.send_video(chat.id, gif, caption=f"{mention(user)} se pone a posar como un jojo.\n\njaja tremendo virgo, no la pone mas", parse_mode=markdown)#['message_id']

@run_async
def slap(bot, update):
	chat, message, user = update.effective_chat, update.effective_message, update.effective_user

	if chat.type == "supergroup":
		user1, user2, self, basic = mention(user), None, None, None

		if message.reply_to_message:
			if message.reply_to_message.from_user.id == bot.id:
				return
			if user.id == message.reply_to_message.from_user.id:
				self = True
			else:
				user2 = mention(message.reply_to_message.from_user)
			reply_msg = message.reply_to_message.message_id
		else:
			try:
				user2 = message.text.split()[1]
			except IndexError:
				basic = True
			else:
				if not user2.startswith("@"):
					user2 = "@" + user2
				if user2 == "@admin" or "/" in user2:
					return 
				if user2[1:] == bot.username:
					return
				if user.username and user2[1:] == user.username:
					self = True
				else:
					if len(user2) < 5 or len(user2) > 32:
						return
					user2 = escape_markdown(user2)
				reply_msg = None

		if self:
			temp = random.choice(slap_strings['self'])
		elif basic:
			temp = random.choice(slap_strings['basic'])
		else:
			temp = random.choice(slap_strings['templates'])

		item, hit, throw = random.choice(slap_strings['items']), random.choice(slap_strings['hit']), random.choice(slap_strings['throw'])
		chat.send_message(temp.format(user1=user1, user2=user2, item=item, hits=hit, throws=throw), markdown, reply_to_message_id=reply_msg)

dispatcher.add_handler(CommandHandler("dan", danbooru_request))
dispatcher.add_handler(CommandHandler("kiss", interact))
dispatcher.add_handler(CommandHandler("pat", interact))
dispatcher.add_handler(CommandHandler("hug", interact))
dispatcher.add_handler(CommandHandler("shoot", interact))
dispatcher.add_handler(CommandHandler("jpose", jpose))
dispatcher.add_handler(CommandHandler("slap", slap))
dispatcher.add_handler(CommandHandler("random", random_choice))
dispatcher.add_handler(CommandHandler("reload", _reload))
dispatcher.add_handler(CommandHandler("ruleta", russian_roulette))
dispatcher.add_handler(CommandHandler("dado", roll_dice))

from .. import dispatcher
from ..utils import *
from ..filters import _filters
from .database import database, user_is, in_db
from telegram import ParseMode
from telegram.error import BadRequest
from telegram.ext import Filters, MessageHandler, CommandHandler
from telegram.ext.dispatcher import run_async
from time import sleep as wait
import time

markdown = ParseMode.MARKDOWN

@run_async
def FilterCommands(bot, update):
	chat, message = update.effective_chat, update.effective_message
	cid = str(chat.id)
	if chat.type == "supergroup":
		if in_db(chat) and database['chats'][cid]['settings']['delete_commands']: 
			try:
				message.delete()
			except BadRequest:
				pass

@run_async
def Toggle(bot, update):
	chat, user, message = update.effective_chat, update.effective_user, update.effective_message
	cid = str(chat.id)
	if user.id in admins(bot, chat.id) and chat.type == "supergroup":
		if in_db(chat):
			settings = database['chats'][cid]['settings']
			val = message.text.split()[1]
			if val in settings:
				if type(settings[val]) is bool:
					settings[val] = not settings[val]
					bot.send_message(chat.id, f"Valor para `{val}` actualizado de `{not settings[val]}` a `{settings[val]}`", markdown)
				else:
					bot.send_message(chat.id, f"`{val}` no es un booleano!\n{type(settings[val])}")
			else:
				bot.send_message(chat.id, "La opción no existe!")

@run_async
def Update_Setting(bot, update):
	chat, user, message = update.effective_chat, update.effective_user, update.effective_message
	cid = str(chat.id)
	if user.id in admins(bot, chat.id) and chat.type == "supergroup" and in_db(chat):
		cmd = (message.text).split(None, 2)
		if cmd[1] in database['chats'][cid]['settings']:
			settings, new_val = database['chats'][cid]['settings'], cmd[2]
			if type(settings[cmd[1]]) is str:
				settings[cmd[1]] = new_val
				bot.send_message(chat.id, f"Se ha actualizado el valor de la opción `{cmd[1]}`", markdown)
		else:
			print("key not found")

@run_async
def Welcome(bot, update):
	chat, user, message = update.effective_chat, update.effective_user, update.effective_message
	cid = str(chat.id)
	if in_db(chat) and database['chats'][cid]['settings']["welcome"]:
		if message.new_chat_members:
			welcome_message = (database['chats'][cid]['settings']['welcome_message']).format(group_title=chat.title,user=mention(user),usr_id=f'`{user.id}`',username=user.username)
			bot.send_message(chat.id, welcome_message, parse_mode=markdown, reply_to_message_id=message.message_id)

@run_async
def Rules(bot, update):
	chat, user, message = update.effective_chat, update.effective_user, update.effective_message
	cid = str(chat.id)
	if chat.type == "supergroup" and in_db(chat):
		if message.reply_to_message:
			reply_id = message.reply_to_message.message_id
		else:
			reply_id = None
		bot.send_message(chat.id, database['chats'][cid]['settings']['rules'], parse_mode=markdown, reply_to_message_id=reply_id)

@run_async
def UnbanKick(bot, update):
	chat, user, message = update.effective_chat, update.effective_user, update.effective_message

	if (message.text).startswith("/pardon") or (message.text).startswith("/unban"):
		action = "desbaneado"
	else:
		action = "expulsado"

	if user.id in admins(bot, chat.id) or user_is(user, 'moderator', 'chat', chat):
		if message.reply_to_message:
			replied_user = message.reply_to_message.from_user
			if replied_user.id == user.id or replied_user.id in admins(bot, chat.id) or user_is(replied_user, 'moderator', 'chat', chat):
				return
			else:
				chat.unban_member(replied_user.id)
				bot.send_message(chat.id, "{} ha sido {}.".format(mention(replied_user), action), markdown)
	else:
		bot.send_message(chat.id, "admin only!")

@run_async
def SpamCheck(bot, update):
	chat, user, message = update.effective_chat, update.effective_user, update.effective_message
	cid, uid = str(chat.id), str(user.id)
	if in_db(chat) and uid in database['chats'][cid]['user_stats']:
		if message.text == "+" or message.text == "-":
			message.delete()

		split_message = message.text.split()

		for x in split_message:
			for command in _filters['combot_cmds']:
				if message.text == command:
					message.delete()

			if not user.id in admins(bot, chat.id) or not database['chats'][cid]['user_stats'][uid]['moderator']:
				if any(keyword in x.lower() for keyword in _filters['urls']):
					message.delete()
					message_text = "¡Hey {}!\nLos acortadores de enlaces no estan permitidos, si vas a enviar enlaces entonces hazlo de manera directa."
					bot.send_message(chat.id, message_text.format(mention(user)), markdown)

@run_async
def DeleteMessage(bot, update):
	chat, user, message = update.effective_chat, update.effective_user, update.effective_message
	cid, uid = str(chat.id), str(user.id)
	if user.id in admins(bot, chat.id) or database['chats'][cid]['user_stats'][uid]['moderator']:
		if message.reply_to_message:
			try:
				bot.delete_message(chat.id, message.reply_to_message.message_id)
			except BadRequest:
				pass

@run_async
def toggle_admin(bot, update):
	user, chat = update.effective_user, update.effective_chat

	if str(user.id) in database['users'] and user_is(user, 'developer'):
		if user.id in admins(bot, chat.id):
			bot.promote_chat_member(chat.id, user.id, None, None, None, False, False, False)
			msg = bot.send_message(chat.id, "Eliminado de administradores")["message_id"]
			wait(10)
			bot.delete_message(chat.id, msg)
		else:
			bot.promote_chat_member(chat.id, user.id, None, None, None, True, True, True)
			msg = bot.send_message(chat.id, "Añadido a administradores")["message_id"]
			wait(10)
			bot.delete_message(chat.id, msg)

def extract_time(message, time_val):
	if any(time_val.endswith(unit) for unit in ('m', 'h', 'd', 'y')):
		unit = time_val[-1]
		time_num = time_val[:-1]  # type: str
		if not time_num.isdigit():
			return int(time.time() + 0)

		if unit == 'm':
			bantime = int(time.time() + int(time_num) * 60)
		elif unit == 'h':
			bantime = int(time.time() + int(time_num) * 60 * 60)
		elif unit == 'd':
			bantime = int(time.time() + int(time_num) * 24 * 60 * 60)
		elif unit == 'y':
			bantime = int(time.time() + int(time_num) * 365 * 24 * 60 * 60)
		else:
			return ""
		return bantime
	else:
		return 0

@run_async
def tmute(bot, update):
	chat, user, message = update.effective_chat, update.effective_user, update.effective_message
	cid, uid = str(chat.id), str(user.id)
	if in_db(chat) and user_is(user, 'moderator', "chat", chat) or user.id in admins(bot, chat.id):
		if message.reply_to_message:
			rep, cmd = message.reply_to_message.from_user, (message.text).split(None, 2)

			if len(cmd) > 1:
				time = cmd[1]
			else:
				time = "tiempo ilimitado"

			if len(cmd) > 2:
				reason = "Razón: {}".format(cmd[2])
			else:
				reason = "Sin especificar."

			if rep.id in admins(bot, chat.id):
				return

			member, mute_time = chat.get_member(rep.id), extract_time(message, time)
			if member.can_send_messages is None or member.can_send_messages:
				bot.restrict_chat_member(chat.id, rep.id, until_date=mute_time, can_send_messages=False)
				chat.send_message(f"{mention(rep)} ha sido silenciado durante {time}.\n\nRazón: {reason}", markdown)
			else:
				chat.send_message("Este usuario ya está silenciado!")

@run_async
def unmute(bot, update):
	chat, user, message = update.effective_chat, update.effective_user, update.effective_message
	cid, uid = str(chat.id), str(user.id)
	if in_db(chat) and user_is(user, 'moderator', "chat", chat) or user.id in admins(bot, chat.id):
		if message.reply_to_message:
			rep = message.reply_to_message.from_user
			member = chat.get_member(rep.id)
			if member.can_send_messages is None or member.can_send_messages:
				chat.send_message("Este usuario no esta silenciado!")
			else:
				bot.restrict_chat_member(chat.id, rep.id, until_date=time.time()+1, can_send_messages=True, can_send_media_messages=True, can_send_polls=True, can_send_other_messages=True, can_add_web_page_previews=True)
				chat.send_message(f"{mention(rep)} ha sido desilenciado.", markdown)
				
@run_async
def ban(bot, update):
	chat, user, message = update.effective_chat, update.effective_user, update.effective_message
	if user.id in admins(bot, chat.id):
		if message.reply_to_message:
			replied_user = message.reply_to_message.from_user
			if replied_user.id == user.id or replied_user.id == bot.id or replied_user.id in admins(bot, chat.id):
				return
			chat.kick_member(replied_user.id)
			bot.send_message(chat.id, "{} ha sido baneado.".format(mention(replied_user)), markdown)
		else:
			spt_id = message.text.split()[1]
			if spt_id:
				usr = int(spt_id)
				if usr in admins(bot, chat.id):
					return
				else:
					chat.kick_member(usr)
					bot.send_message(chat.id, "Usuario Baneado!")
	else:
		bot.send_message(chat.id, "Admin-only!")

@run_async
def Mod(bot, update):
	user, msg, chat = update.effective_user, update.effective_message, update.effective_chat

	reply = msg.reply_text

	if user.id in admins(bot, chat.id):
		if msg.reply_to_message:
			replied_user = msg.reply_to_message.from_user
			if replied_user.id == bot.id or replied_user.id == user.id:
				return
			else:
				if user_is(replied_user, 'moderator', "chat", chat):
					database['chats'][str(chat.id)]['user_stats'][str(replied_user.id)]['moderator'] = False
					bot.send_message(chat.id, "{} ha sido relevado de su cargo como moderador.".format(mention(replied_user)), markdown)
				else:
					database['chats'][str(chat.id)]['user_stats'][str(replied_user.id)]['moderator'] = True
					bot.send_message(chat.id, "{} ha sido promovido a moderador.".format(mention(replied_user)), markdown)

# Handlers
dispatcher.add_handler(CommandHandler("toggle_admin", toggle_admin))
dispatcher.add_handler(CommandHandler("toggle", Toggle))
dispatcher.add_handler(CommandHandler("rules", Rules))
dispatcher.add_handler(CommandHandler("set", Update_Setting))
dispatcher.add_handler(CommandHandler("mute", tmute))
dispatcher.add_handler(CommandHandler("unmute", unmute))
dispatcher.add_handler(CommandHandler("mod", Mod))
dispatcher.add_handler(CommandHandler("ban", ban))
dispatcher.add_handler(CommandHandler("kick", UnbanKick))
dispatcher.add_handler(CommandHandler("unban", UnbanKick))
dispatcher.add_handler(CommandHandler("pardon", UnbanKick))
dispatcher.add_handler(CommandHandler("del", DeleteMessage))
dispatcher.add_handler(MessageHandler(Filters.group and Filters.status_update.new_chat_members, Welcome), 3)
dispatcher.add_handler(MessageHandler(Filters.group and Filters.command, FilterCommands), 4)
dispatcher.add_handler(MessageHandler(Filters.group and Filters.text, SpamCheck), 5)
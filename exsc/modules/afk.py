from .. import dispatcher
from ..utils import *
from .database import database, in_db
from telegram import ParseMode
from telegram.ext import Filters, MessageHandler, CommandHandler
from telegram.ext.dispatcher import run_async
from telegram.utils.helpers import escape_markdown
markdown = ParseMode.MARKDOWN

@run_async
def setAFK(bot, update):
	chat, user, message = update.effective_chat, update.effective_user, update.effective_message
	uid = str(user.id)
	if in_db(user) and not database['users'][uid]['afk']['status']:
		database['users'][uid]['afk']['status'] = True
		if len((message.text).split()) > 1:
			reason = (message.text).split(None, 1)[1]
		else:
			reason = "Sin especificar"
		database['users'][uid]['afk']['reason'] = reason
		bot.send_message(chat.id, f"{mention(user)} ahora está ausente.\n\nRazón: {reason}.", markdown)

@run_async
def CheckAfks(bot, update):
	chat, user, message = update.effective_chat, update.effective_user, update.effective_message
	uid = str(user.id)
	if message.reply_to_message:
		user2 = message.reply_to_message.from_user
		u2id = str(user2.id)
		if user.id != user2.id and in_db(user2) and database['users'][u2id]['afk']['status']:
			bot.send_message(chat.id, f"¡Hey {mention(user)}!\n{user2.full_name} no se encuenta ahora mismo.\n\nRazón: {database['users'][u2id]['afk']['reason']}", markdown, reply_to_message_id=message.message_id)

	if in_db(user) and database['users'][uid]['afk']['status']:
		database['users'][uid]['afk']['status'] = False
		bot.send_message(chat.id, f"¡{mention(user)} ha regresado!", markdown)

	for _user in database['users']:
		username = database['users'][_user]['username']
		if database['users'][_user]['afk']['status'] and username is not None and f"@{username}" in (message.text).split():
			print(f"¡Hey {mention(user)}!\n{database['users'][_user]['username']} no se encuenta ahora mismo.\n\nRazón: {database['users'][_user]['afk']['reason']}")
			bot.send_message(chat.id, f"¡Hey {mention(user)}!\n{escape_markdown(database['users'][_user]['username'])} no se encuenta ahora mismo.\n\nRazón: {database['users'][_user]['afk']['reason']}", markdown, reply_to_message_id=message.message_id)


dispatcher.add_handler(CommandHandler("afk", setAFK))
dispatcher.add_handler(MessageHandler(Filters.group and Filters.text, CheckAfks))

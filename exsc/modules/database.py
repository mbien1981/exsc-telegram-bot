# This may not be the best 'database' in the world, but it does it's job #
from .. import dispatcher
from telegram.ext import Filters, MessageHandler, CommandHandler
from telegram.ext.dispatcher import run_async
import json, threading
global database

def load_database(): 
	with open('data.json') as json_file:
		try:
			data = json.load(json_file)
		except ValueError:
			data = {}
		return data

database = load_database()

def setup_database():
	if not "chats" in database:
		database["chats"] = {}
	if not "users" in database:
		database["users"] = {}
setup_database()


def save_database(): 
	threading.Timer(5.0, save_database).start()
	with open('data.json', 'w') as json_file:
		json.dump(database, json_file, indent=4, sort_keys=True)
save_database()

def in_db(data):
	if hasattr(data, 'title'):
		if str(data.id) in database['chats']:
			return True
	else:
		if str(data.id) in database['users']:
			return True

def user_is(user, key, tree="user", chat="None"):
	if tree == "user":
		if in_db(user):
			if database['users'][str(user.id)][key]:
				return True
	elif tree == "chat" or tree == "chats":
		if in_db(chat) and str(user.id) in database['chats'][str(chat.id)]['user_stats']:
			if database['chats'][str(chat.id)]['user_stats'][str(user.id)][key]:
				return True

@run_async
def add_user_to_database(bot, update):
	chat, user = update.effective_chat, update.effective_user
	cid, uid = str(chat.id), str(user.id)
	if uid in database['users']:
		usr = database['users'][uid]
		if usr['username'] != user.username:
			usr['username'] = user.username
	else:
		database['users'][uid] = {
			'developer': False,
			'username': user.username,
			'afk': {
				"status": False,
				"reason": None
			}
		}
		print(f'{user.full_name} ({user.id}) added to database')

	bchat = database['chats']
	if cid in bchat:
		if uid in bchat[cid]['user_stats']:
			bchat[cid]['user_stats'][uid]['messages']+=1
		else:
			bchat[cid]['user_stats'][uid] = {
				'moderator': False,
				'messages': 0,
				"warns": 0,
				"warnings": []
			}
			print(f"user stats created for {user.full_name} ({user.id}) in {chat.id}")

@run_async
def add_chat_to_database(bot, update):
	chat = str(update.effective_chat.id)
	if not chat in database['chats']:
		database['chats'][chat] = {
			"title": update.effective_chat.title,
			"settings": {
				"welcome": True,
				"welcome_message": "¡Hola {user}! ¡Bienvenido a {group_title}!",
				"rules": "No se han establecido reglas!",
				"delete_commands": True,
				"allow_nsfw": False
			},
			'user_stats': {}
		}
		print(f"{update.effective_chat.title} ({chat}) added to db")
		bot.send_message(update.effective_chat.id, "¡Gracias por agregarme a este grupo!\n\nSi ves este mensaje es probable que sea la primera vez que estoy en este grupo o el desarrollador ha decidido limpiar la base de datos.\n\n⚙ Desarrollador: @Dampfveteran")

@run_async	
def export(bot, update):
	chat, user = update.effective_chat, update.effective_user
	if chat.type == "private":
		bot.send_document(chat.id, document=open('data.json', 'rb'))

dispatcher.add_handler(CommandHandler("export", export))
dispatcher.add_handler(MessageHandler(Filters.group, add_chat_to_database), 1)
dispatcher.add_handler(MessageHandler(Filters.all, add_user_to_database), 2)
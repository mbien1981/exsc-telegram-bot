from . import VERSION, dispatcher
from telegram import ParseMode, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CommandHandler
from telegram.ext.dispatcher import run_async
from telegram.utils.helpers import escape_markdown
from urllib.parse import urlparse
markdown = ParseMode.MARKDOWN

def admins(bot, chat_id): # Get a list with all admins' IDs.
	return [admin.user.id for admin in bot.get_chat_administrators(chat_id)]

def mention(user):
	return f"[{escape_markdown(user.full_name)}](tg://user?id={user.id})"

def build_menu(buttons, n_cols, header_buttons=None, footer_buttons=None):
	menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
	if header_buttons:
		menu.insert(0, [header_buttons])
	if footer_buttons:
		menu.append([footer_buttons])
	return menu

@run_async
def start(bot, update):
	chat, user = update.effective_chat, update.effective_user
	if chat.type == "private":
		buttons = [InlineKeyboardButton(text="TEST BUTTON",url="google.com/search?q=telegram")]
		reply_markup = InlineKeyboardMarkup(build_menu(buttons, n_cols=2))
		bot.send_message(chat.id, f"¡Hola {mention(user)}!\n\n⚙Versión: {VERSION}", markdown, reply_markup=reply_markup)

dispatcher.add_handler(CommandHandler("start", start))
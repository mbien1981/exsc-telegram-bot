Notes: 
    This bot is meant to be hosted in heroku.com, you may have to change some stuff in case you want to use another platform.
    In case you rename the 'exsc' folder, you must edit Procfile and __main__.py files.

Thanks to uh_wot (https://gitlab.com/uh_wot) who helped me a lot during the developement of this telegram bot.

entertainment of commands:

'afk' notify a user that you are absent, use: /afk [reason], ex: /afk sleeping
'dan' get an image from danbooru, use: /dan [tag], ex: /dan astolfo_(fate)
['slap', 'kiss', 'shoot', 'hug', 'pat'] 'Interact with a user', use: reply to a message with any of these commands.
'jpose' do a jojo pose, use: type /jpose in the chat
'random' let the bot choose a random word from your message, use: /random [options], ex: /random apple potato banana
['reload', 'ruleta'] play russian roulette, use: the weapon must be loaded (use /reload), then use /ruleta
'dado' roll a dice, use: /dado [optional: amount, faces], ex: /dado 3 12 (3 dices of 12 faces)

sticker commands:

'kang' steal a sticker/image and add it to your own kang pack, use: [reply to a message with media] /kang [optional emojis], ex: /kang 🇮🇹
'getsticker' get the .png file of a sticker, use: (reply to a sticker) /getsticker

admin commands:

'toggle_admin' toggles the admin permissions of the bot developer (made for testing purposes)
'toggle' toggle a chat setting from true to false and viceversa, use: /toggle [option], ex: /toggle delete_commands
'set' change the value of a text option from the chat, use: /set [option], ex: /set rules do not post any kind of pornography
'rules' get chat rules
'mute' mute a group member, use: [reply to a message] /mute [optional: time reason], ex: [reply to a message] /mute 1d spam
'unmute' unmute a muted group member (duh)
'mod' promote a group member to moderator role
'ban' ban a user from your group, use: (reply to a message) /ban (optional: user_id), ex: /ban 950475564
'kick' kick a user from your group, use: (reply to a message) /kick
['unban' 'pardon'] unban a banned user from your group (duh)
'del' delete a message from your chat, use: (reply to a message) /del